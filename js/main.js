$(document).ready(function(){



$( '.review-helpful-btn' ).on( 'click', function( event ) {

	var $target = $( event.currentTarget );
       
    val = $target.attr( 'data-value' );
	
	val = ++val;
	
	$target.attr( 'data-value', val );
	
	$target.find('span').text(val);

   
});


$( '.outside-tab-link' ).on( 'click', function( event ) {

   var $tabtarget = $( event.currentTarget );
       
       val = $tabtarget.attr( 'href' );


   if( val !== undefined ){
      		
      		$tabtoshow = val.slice(0,-1);
      		
      		alert($tabtoshow);

      		$('a[href=\"'+ $tabtoshow + '\"]').tab('show');
      
      		


      $('html, body').animate({
      
              scrollTop: $($tabtoshow).offset().top
              
      }, 1000 );
      
      val = undefined;
      	
       
      }
   
});	
	
  
  var options = [];

$( '#filter .dropdown-menu a' ).on( 'click', function( event ) {

   var $target = $( event.currentTarget ),
       val = $target.attr( 'data-value' ),
       $inp = $target.find( 'input' ),
       idx;

   if ( ( idx = options.indexOf( val ) ) > -1 ) {
      options.splice( idx, 1 );
      setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
   } else {
      options.push( val );
      setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
   }

   $( event.target ).blur();
      
   console.log( options );
   return false;
});
  
  
  $( '#promo-btn' ).click(function() {
  	
  	$( '#promo-btn' ).addClass('hidden');
  	$( '#promo-search' ).removeClass('hidden');
  	
  });
  
  
  $( '#promo-search-close' ).click(function() {
  	
  	$( '#promo-search' ).addClass('hidden');
  	$( '#promo-btn' ).removeClass('hidden');
  	
  });
  
  
  $('#main-play-btn').click(function() {
  

    $('#popup-video').modal({

    backdrop: 'static',
    keyboard: false

}).modal('show');

  	$('#popup-video').removeClass('pause');
  	
  	var movieid = "#" + $(this).attr('movie-data');  	
  	
  	$(movieid).removeClass('hidden').trigger('play');
  		
  });
  
  
  $('#popup-video .close-btn').click(function() {
  
      	$('#popup-video video').trigger('pause');
    	$('#popup-video').addClass('pause');
		$('#popup-video').modal('hide');
  		
  });

	
 $('.howmoviestart').click(function () {

    $('#popup-video').modal({

    backdrop: 'static',
    keyboard: false

}).modal('show');

  	$('#popup-video').removeClass('pause');
  	
  	var movieid = "#" + $(this).attr('movie-data');  	
  	
  	$(movieid).removeClass('hidden').trigger('play');

});
 

  $('.attend-register-btn').click(function(){
   
    if ( $(this).closest('.attend-row').is('.active') ) {
    
      
        $(this).closest('.attend-row').removeClass('active');

        $(this).closest('.attend-register-btn').text('register');
        


    }
    
    else{
    
        $('.attend-register-btn').text('register');
        
        $(this).closest('.attend-register-btn').text('unregister');
      
        $('.attend-row').removeClass('active');
        
        $('#add-attend-message').removeClass('hidden');
        
        $(this).closest('.attend-row').addClass('active');
    
    }
    
});

 
 $('video').click(function(){
 
	 if (this.paused) {
	 
	 		$(this).closest('#popup-video').removeClass('pause');
		   	this.play();
	       
	 } else {
	       this.pause();
	 		$(this).closest('#popup-video').addClass('pause');

	 }

});
 

$('#dropdown-menu-beds .check-all').click(function(event){
 	
 	event.stopPropagation();
	 $('#dropdown-menu-beds input:checkbox').prop('checked', 'checked');
	 $('#dropdown-menu-beds .clear-all').removeClass('hidden');
	 $(this).addClass('hidden');

});

$('#dropdown-menu-beds .clear-all').click(function(event){
 	
 	event.stopPropagation();
	 $('#dropdown-menu-beds input:checkbox').prop('checked', '');
	 $(this).addClass('hidden');
	 $('#dropdown-menu-beds .check-all').removeClass('hidden');

});

$('#dropdown-menu-baths .check-all').click(function(event){
 	
 	event.stopPropagation();
	 $('#dropdown-menu-baths input:checkbox').prop('checked', 'checked');
	 $('#dropdown-menu-baths .clear-all').removeClass('hidden');
	 $(this).addClass('hidden');

});

$('#dropdown-menu-baths .clear-all').click(function(event){
 	
 	event.stopPropagation();
	 $('#dropdown-menu-baths input:checkbox').prop('checked', '');
	 $(this).addClass('hidden');
	 $('#dropdown-menu-baths .check-all').removeClass('hidden');

});





  $(window).on('scroll', function(){		

  	var yPos = -( $(window).scrollTop() / 5 );
  	$('.hero-image').css('margin-top', -550 + yPos);
  	
  });

  //------------------------------------//
  //Navbar//
  //------------------------------------//
    	var menu = $('.navbar');
    	$(window).bind('scroll', function(e){
    		if($(window).scrollTop() > 140){
    			if(!menu.hasClass('open')){
    				menu.addClass('open');
    				
    				
    			}
    		}else{
    			if(menu.hasClass('open')){
    				menu.removeClass('open');
    				
    			}
    		}
    	});
  
  //------------------------------------//
  //Scroll To//
  //------------------------------------//
  $(".scroll").click(function(event){		
  
  	event.preventDefault();
  	$('html,body').animate({scrollTop:$(this.hash).offset().top}, 800);
  	
  });
  
  
  $.fn.extend({
    animateCss: function (animationName) {
    
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        
        $(this).addClass('animated ' + animationName).one(animationEnd, function() {
        
            $(this).removeClass('animated' + animationName);
        
        });
        
    }
});
  
  //------------------------------------//
  //Wow Animation//
  //------------------------------------// 
  wow = new WOW(
        {
          boxClass:     'wow',      // animated element css class (default is wow)
          animateClass: 'animated', // animation css class (default is animated)
          offset:       0,          // distance to the element when triggering the animation (default is 0)
          mobile:       false        // trigger animations on mobile devices (true is default)
        }
      );
      wow.init();


	
});



$("[data-gui]").each(function(index) {

    $(this).on("click", function(event){
      
      $targethide = $( event.currentTarget );
      
      hidetarget = $targethide.data( 'hide-target' );
      showtarget = $targethide.data( 'show-target' );



      if( hidetarget !== undefined ){
      
          $(hidetarget).addClass('hidden');
          
      }

      if( showtarget !== undefined ){
      
          	$(showtarget).removeClass('hidden');
          
      }
      
      })

});
