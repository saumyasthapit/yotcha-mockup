$(document).on("click", ".skip-guide" , function(){

	$(this).parents(".popover.tour-tour").popover('destroy');
        
});

var tour = new Tour({
  steps: [

  {
    element: "#eap1",
    title: "Step 1/13",
	animation: false,
    content: "Hello, My name is Yani. I will be here to guide you through filling out the agreement. You will have the opportunity to review the complete agreement when we are done.",
    template: '<div class="container-fluid popover" data-animation="false" role="tooltip">	<div class="row">	<div class="col-sm-6 col-sm-offset-3">	<img src="img/support-icon.png" class="yani" />	<h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation" style="margin-left: 183px">	<button class="btn btn-sm btn-primary" data-role="next">Next</button>	<button class="btn btn-sm btn-custom skip-guide" data-role="end">Skip Guide</button>  </div> </div>	</div>	</div>'
  },
  
  {
    element: "#eap2",
    title: "Step 2/13",
	animation: false,
    content: "Let’s start. You may click ‘Back’ at any time to return to the previous step or ‘Skip Guide’ to fill out the agreement on your own. If you’re ready, just click ‘Next’."
  },
  
  {
    element: "#eap3",
	animation: false,
	smartPlacement: false,
    title: "Step 3/13",
    content: "Please check that the Estate Agent Name and Licence No. are correct, and enter your company mailing address."
  },
  
  {
    element: "#eap4",
	animation: false,
    title: "Step 4/13",
    content: "Please check the property address. The full address will be displayed after the Owner accepts your bid."
  },
  
  {
    element: "#eap5",
	animation: false,
    title: "Step 5/13",
    content: "This Exclusive Agreement will commence once the Owner accepts your bid. It shall expire XX days from the Agreement Date."
  },

  {
    element: "#eap6",
	animation: false,
    title: "Step 6/13",
    content: "Please check the Expected Sale Price."
  },
  
  {
    element: "#eap7",
	animation: false,
    title: "Step 7/13",
    content: "Please check the agent commission rate for this property. Please indicate whether GST is payable upon the commission (whether your company is GST registered). If Yes, please indicate whether the commission specified is inclusive of GST."
  },
  
  {
    element: "#eap8",
	animation: false,
    title: "Step 8/13",
    content: "Please indicate whether you or your company has a conflict or potential conflict of interest in acting for the Owner. If so, please provide details in the text box below."
  },
  
  {
    element: "#eap9",
	animation: false,
    title: "Step 9/13",
    content: "Please read and confirm that you agree to any Additional Terms that the Owner has stipulated."
  },
  
    {
    element: "#eap10",
 	animation: false,
    title: "Step 10/13",
    content: "Please check your name and input your NRIC No."
  },
  
  {
    element: "#eap11",
	animation: false,
    title: "Step 11/13",
    content: "Please check the phone number you have provided in your Agent Profile."
  },
  
  {
    element: "#eap12",
	animation: false,
    title: "Step 12/13"
  },
  
  {
    element: "#eap13",
	animation: false,
    title: "Step 13/13",
    next: 1,
    content: "Please review the agreement. Once you’re ready, click here to proceed to authorize payment. Payment will only be processed when you secure the exclusive deal.",
    template: '<div class="container-fluid popover" data-animation="false" role="tooltip">	<div class="row">	<div class="col-sm-6 col-sm-offset-3">	<h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <button class="btn btn-sm btn-default" data-role="prev">Back</button> <button class="btn btn-sm btn-primary" data-role="next">Review agreement</button> </div>  </div> </div>	</div>'
  }

  
]});

tour.restart();

// Initialize the tour
tour.init();

// Start the tour
tour.start();